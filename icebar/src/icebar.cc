#include "config.h"
#undef CONFIG_GUIEVENTS
#include "ylocale.h"
#include "yxapp.h"
#include "atray.h"
#include "ref.h"

const char *ApplicationName = "icebar";

int main(int argc, char **argv) {
    YXApplication app(&argc, &argv);

    YLocale bob("");

    TrayApp *tray = new TrayApp(0);
    tray->show();

    return app.mainLoop();
}

// vim: set sw=4 ts=4 et:
