#ifndef __CLOCK_H
#define __CLOCK_H

#include "ywindow.h"
#include "ytimer.h"

#ifdef CONFIG_APPLET_CLOCK

class XSMListener;

class YClock: public YWindow, public YTimerListener {
public:
    YClock(XSMListener *smActionListener, YWindow *aParent = 0);
    virtual ~YClock();

    void autoSize();

    virtual void handleButton(const XButtonEvent &button);
    virtual void handleCrossing(const XCrossingEvent &crossing);
    virtual void handleClick(const XButtonEvent &up, int count);
    virtual void paint(Graphics &g, const YRect &r);

    void updateToolTip();
    virtual bool handleTimer(YTimer *t);

private:
    YTimer *clockTimer;
    bool clockUTC;
    bool toolTipUTC;
    int transparent;
    XSMListener *smActionListener;

    ref<YPixmap> getPixmap(char ch);
    int calcWidth(const char *s, int count);
    bool hasTransparency();


    static YColor *clockBg;
    static YColor *clockFg;
    static ref<YFont> clockFont;
};
#endif

#endif
