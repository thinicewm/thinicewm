# NAME

    icewm-menu-gnome2 - 

# SYNOPSIS

**** **icewm-menu-gnome2** \[_OPTIONS_\]
 **icewm-menu-gnome2** {**-h**|**--help**} \[_OPTIONS_\]
 **icewm-menu-gnome2** {**-V**|**--version**}
 **icewm-menu-gnome2** {**-C**|**--copying**}

# DESCRIPTION

# OPTIONS

**icewm-menu-gnome2** recognizes the following options:

## COMMAND OPTIONS

Command options are mutually exclusive.  Only one command option can be
specified per invocation.  If no command option is specified, argument
parsing and processing is performed.

- **-h**, **--help**

    Print a brief usage statement to `stdout` and exit.

- **-V**, **--version**

    Print the program version to `stdout` and exit.

- **-C**, **--copying**

    Print copying permissions to `stdout` for the program and exit.

## GENERAL OPTIONS

**icewm-menu-gnome2** recognizes the two following mutually exclusive
options:

- **--open** _FILENAME_

    Read the GNOME version 2 `.desktop` file specified by _FILENAME_ and
    execute the `Exec` line with variable substitution.  This is used when
    generating menus to launch individual items within the menu.

- **--list** _DIRECTORY_

    Read the GNOME version 2 _DIRECTORY_ and generate [icewm(1)](http://man.he.net/man1/icewm) menu
    items for each file or sub-directory within the _DIRECTORY_.
    Sub-directories are not immediately expanded but generate **menuprog**
    entries executing **icewm-menu-gnome2** with the sub-directory as the
    _DIRECTORY_ argument.

# USAGE

This utility is not normally used directly, but is used as the
executable in a **menuprog** entry in a menu.  It is quite deprecated.
Use [icewm-menu-fdo(1)](http://man.he.net/man1/icewm-menu-fdo) instead.

# CONFORMING TO

**icewm-menu-gnome2** complies to the GNOME version 2 menu specification.
This is somewhat different from the XDG desktop file specification, and
is quite deprecated.

# CAVEATS

The **icewm-menu-gnome2** program is only built when the [icewm(1)](http://man.he.net/man1/icewm)
package is configured with the **--enable-menus-gnome2** option.

# SEE ALSO

[icewm(1)](http://man.he.net/man1/icewm),
[icewm-menu-fdo(1)](http://man.he.net/man1/icewm-menu-fdo),
[icewm-preferences(5)](http://man.he.net/man5/icewm-preferences).

# BUGS

**icewm-menu-gnome2** had no known bugs at the time of release.  Please report bugs
for current versions to the source code repository at
[https://github.com/bbidulock/icewm/issues](https://github.com/bbidulock/icewm/issues).

# AUTHOR

Brian Bidulock [mailto:bidulock@openss7.org](mailto:bidulock@openss7.org).

See **--copying** for full copyright notice and copying permissions.

# LICENSE

**IceWM** is licensed under the GNU Library General Public License.
See the `COPYING` file in the distribution or use the **--copying** flag
to display copying permissions.
