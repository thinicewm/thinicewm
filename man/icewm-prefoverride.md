# NAME

    icewm-prefoverride - icewm override preferences configuration file

# SYNOPSIS

`` `$ICEWM_PRIVCFG/prefoverride`
 `$XDG_CONFIG_HOME/icewm/prefoverride`
 `$HOME/.icewm/prefoverride`
 `/etc/icewm/prefoverride`
 `/usr/share/icewm/prefoverride`

# DESCRIPTION

Settings which override the settings from a theme.  Some of the **icewm**
configuration options from the preferences file which control the
look-and-feel may be overridden by the theme, if the theme designer
thinks this is desirable.  However, this `prefoverride` file will again
override this for a few specific options of your choosing.  It is safe
to leave this file empty initially.

# FORMAT

# EXAMPLES

# FILES

Locations for the toolbar options file are as follows:

- `$ICEWM_PRIVCFG/prefoverride`
- `$XDG_CONFIG_HOME/icewm/prefoverride`
- `$HOME/.icewm/prefoverride`
- `/etc/icewm/prefoverride`
- `/usr/share/icewm/prefoverride`

# SEE ALSO

[icewm(1)](http://man.he.net/man1/icewm).

# AUTHOR

Brian Bidulock [mailto:bidulock@openss7.org](mailto:bidulock@openss7.org).

# LICENSE

**IceWM** is licensed under the GNU Library General Public License.
See the `COPYING` file in the distribution.
