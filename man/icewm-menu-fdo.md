# NAME

    icewm-menu-fdo - 

# SYNOPSIS

**icewm-menu-fdo** \[_FILENAME_\]
 **icewm-menu-fdo** {**-h**|**--help**}
 **icewm-menu-fdo** {**-V**|**--version**}
 **icewm-menu-fdo** {**-C**|**--copying**}

# DESCRIPTION

# ARGUMENTS

- \[_FILENAME_\]

    When provided with the optional _FILENAME_ argument that is the name
    and location of a `.desktop` file, **icewm-menu-fdo** will open an
    launch the application using the `Exec` line from the desktop file.
    This is similar to [icewm-menu-gnome2(1)](http://man.he.net/man1/icewm-menu-gnome2), which is now deprecated.

# OPTIONS

**icewm-menu-fdo** recognizes the following options:

## COMMAND OPTIONS

Command options are mutually exclusive.  Only one command option can be
specified per invocation.  If no command option is specified, argument
parsing and processing is performed.

- **-h**, **--help**

    Print a brief usage statement to `stdout` and exit.

- **-V**, **--version**

    Print the program version to `stdout` and exit.

- **-C**, **--copying**

    Print copying permissions to `stdout` for the program and exit.

## GENERAL OPTIONS

**icewm-menu-fdo** does not recognize any general options.

# USAGE

This utility is not normally used directly, but is used as the
executable in a **menuprog** entry in a menu.  It is the preferred
replacement for the deprecated [icewm-menu-gnome2(1)](http://man.he.net/man1/icewm-menu-gnome2) utility.

# ENVIRONMENT

# CONFORMING TO

**icewm-menu-fdo** complies roughly to the XDG `.desktop` file and menu
specification, see ["Desktop Entry Specification"](#desktop-entry-specification), Version 1.2alpha,
2015-03-06 and ["Desktop Menu Specification"](#desktop-menu-specification), Version 1.1-draft, 31
March 2011.

# CAVEATS

The **icewm-menu-fdo** program is only built when the [icewm(1)](http://man.he.net/man1/icewm) package
is configured with the **--enable-menus-fdo** option.

# SEE ALSO

["Desktop Entry Specification"](#desktop-entry-specification),
["Desktop Menu Specification"](#desktop-menu-specification),
[icewm(1)](http://man.he.net/man1/icewm),
[icewm-preferences(5)](http://man.he.net/man5/icewm-preferences).

# BUGS

**icewm-menu-fdo** had no known bugs at the time of release.  Please report bugs
for current versions to the source code repository at
[https://github.com/bbidulock/icewm/issues](https://github.com/bbidulock/icewm/issues).

# AUTHOR

Brian Bidulock [mailto:bidulock@openss7.org](mailto:bidulock@openss7.org).

See **--copying** for full copyright notice and copying permissions.

# LICENSE

**IceWM** is licensed under the GNU Library General Public License.
See the `COPYING` file in the distribution or use the **--copying** flag
to display copying permissions.
