# NAME

    icehelp - a very simple HTML browser

# SYNOPSIS

**icehelp** \[_OPTIONS_\] _FILENAME_

# DESCRIPTION

**icehelp** is a very simple HTML browser that displays the document
specified on the command line.  It is used by [icewm(1)](http://man.he.net/man1/icewm) internall to
display the ["IceWM Manual"](#icewm-manual) and the manual pages.

# ARGUMENTS

- _FILENAME_

    Specifies the absolute or relative path (relative to the current working
    directory), that specifies the HTML file to browse.

# OPTIONS

## GENERAL OPTIONS

- **-h**, **--help**

    Print a brief usage statement to `stdout` and exit.

- **-V**, **--version**

    Print the program version to `stdout` and exit.

- **-C**, **--copying**

    Print copying permissions to `stdout` for the program and exit.

# BUGS

**icehelp** had no known bugs at the time of release.  Please report bugs
for current versions to the source code repository at
[https://github.com/bbidulock/icewm/issues](https://github.com/bbidulock/icewm/issues).

# AUTHOR

Brian Bidulock [mailto:bidulock@openss7.org](mailto:bidulock@openss7.org).

See **--copying** for full copyright notice and copying permissions.

# LICENSE

**IceWM** is licensed under the GNU Library General Public License.
See the `COPYING` file in the distribution or use the **--copying** flag
to display copying permissions.
