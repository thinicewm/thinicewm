# NAME

    icewm-startup - icewm startup configuration file

# SYNOPSIS

`` `$ICEWM_PRIVCFG/startup`
 `$XDG_CONFIG_HOME/icewm/startup`
 `$HOME/.icewm/startup`
 `/etc/icewm/startup`
 `/usr/share/icewm/startup`

# DESCRIPTION

Contains commands to be executed on **icewm** startup.  This is an
executable script with commands to tweak X11 settings and launch some
applications which need to be active whenever **icewm** is started.
It is run by [icewm-session(1)](http://man.he.net/man1/icewm-session) when **icewm** starts.

# FORMAT

# EXAMPLES

# FILES

Locations for the toolbar options file are as follows:

- `$ICEWM_PRIVCFG/startup`
- `$XDG_CONFIG_HOME/icewm/startup`
- `$HOME/.icewm/startup`
- `/etc/icewm/startup`
- `/usr/share/icewm/startup`

# SEE ALSO

[icewm(1)](http://man.he.net/man1/icewm).

# AUTHOR

Brian Bidulock [mailto:bidulock@openss7.org](mailto:bidulock@openss7.org).

# LICENSE

**IceWM** is licensed under the GNU Library General Public License.
See the `COPYING` file in the distribution.
