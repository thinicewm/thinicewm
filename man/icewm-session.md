# NAME

    icewm-session - 

# SYNOPSIS

**icewm-session** \[_OPTIONS_\]

# DESCRIPTION

# OPTIONS

## GENERAL OPTIONS

- **-h**, **--help**

    Print a brief usage statement to `stdout` and exit.

- **-V**, **--version**

    Print the program version to `stdout` and exit.

- **-C**, **--copying**

    Print copying permissions to `stdout` for the program and exit.

# BUGS

**icewm-session** had no known bugs at the time of release.  Please report bugs
for current versions to the source code repository at
[https://github.com/bbidulock/icewm/issues](https://github.com/bbidulock/icewm/issues).

# AUTHOR

Brian Bidulock [mailto:bidulock@openss7.org](mailto:bidulock@openss7.org).

See **--copying** for full copyright notice and copying permissions.

# LICENSE

**IceWM** is licensed under the GNU Library General Public License.
See the `COPYING` file in the distribution or use the **--copying** flag
to display copying permissions.
